<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet/themes/bouquet/layouts/MainLayout.htm */
class __TwigTemplate_8fc48d2780a3a449c7442081c7e3e836b7bb49d2df4c9430c9349a87c3dbb7bb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("styles" => 25, "partial" => 29, "page" => 33, "framework" => 41, "scripts" => 42);
        $filters = array("theme" => 19);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['styles', 'partial', 'page', 'framework', 'scripts'],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html lang=\"en\">

<head>
  <meta charset=\"utf-8\" />
  

    <title>abc</title>

  
  <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"./assets/img/apple-icon.png\">
  <link rel=\"icon\" type=\"image/png\" href=\"./assets/img/favicon.png\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" rel=\"stylesheet\" />
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.1/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">
  <!-- CSS Files -->
  <link href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.min.css");
        echo "\" rel=\"stylesheet\" />
  <link href=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/now-ui-kit.css?v=1.3.0");
        echo "\" rel=\"stylesheet\" />
  <link href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/now-ui-kit.min.css?v=1.3.0");
        echo "\" rel=\"stylesheet\" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/demo/demo.css");
        echo "\" rel=\"stylesheet\" />
  
  ";
        // line 25
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
        // line 26
        echo "</head>

<body class=\"sections-page sidebar-collapse\">
  ";
        // line 29
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 30
        echo "
  <!-- End Navbar -->

         ";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 34
        echo "    

    <!-- End footer -->

  <!--   Core JS Files   -->
  ";
        // line 39
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/script"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 40
        echo "  
  ";
        // line 41
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="' . Request::getBasePath() .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 42
        echo "  ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 43
        echo "</body>

</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet/themes/bouquet/layouts/MainLayout.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 43,  144 => 42,  133 => 41,  130 => 40,  126 => 39,  119 => 34,  117 => 33,  112 => 30,  108 => 29,  103 => 26,  100 => 25,  95 => 23,  90 => 21,  86 => 20,  82 => 19,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<html lang=\"en\">

<head>
  <meta charset=\"utf-8\" />
  

    <title>abc</title>

  
  <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"./assets/img/apple-icon.png\">
  <link rel=\"icon\" type=\"image/png\" href=\"./assets/img/favicon.png\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" rel=\"stylesheet\" />
  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.1/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">
  <!-- CSS Files -->
  <link href=\"{{ 'assets/css/bootstrap.min.css'|theme }}\" rel=\"stylesheet\" />
  <link href=\"{{ 'assets/css/now-ui-kit.css?v=1.3.0'|theme }}\" rel=\"stylesheet\" />
  <link href=\"{{ 'assets/css/now-ui-kit.min.css?v=1.3.0'|theme }}\" rel=\"stylesheet\" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href=\"{{ 'assets/demo/demo.css'|theme }}\" rel=\"stylesheet\" />
  
  {% styles %}
</head>

<body class=\"sections-page sidebar-collapse\">
  {% partial 'nav' %}

  <!-- End Navbar -->

         {% page %}
    

    <!-- End footer -->

  <!--   Core JS Files   -->
  {% partial \"site/script\" %}
  
  {% framework extras %}
  {% scripts %}
</body>

</html>", "C:\\xampp\\htdocs\\bouquet/themes/bouquet/layouts/MainLayout.htm", "");
    }
}
