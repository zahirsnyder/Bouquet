<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet/themes/bouquet/pages/home.htm */
class __TwigTemplate_174cb4063b18905155f5145283a6b00c2edf02310e9ffae2de50659476a4493d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 2);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"page-header page-header-small\">
        <div class=\"page-header-image\" style=\"filter: brightness(80%);background-image: url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo ");\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-7 ml-auto text-right\">
                    <h1 class=\"title\">History of surfing</h1>
                    <h4 class=\"description\">The riding of waves has likely existed since humans began swimming in the ocean. In this sense, bodysurfing is the oldest type of wave-catching. Standing up on what is now called a surfboard is a relatively recent innovation developed by the Polynesians.</h4>
                    <br>

                    <div class=\"buttons\">
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-get-pocket\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-info btn-lg mr-3\">
                            Read More
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
      <!--     section 2     -->
    <div class=\"features-1\">

            <div class=\"row\">
                <div class=\"col-md-8 ml-auto mr-auto\">
                    <h2 class=\"title\">RARE GIFTS FOR YOUR FAVOURITE PEOPLE</h2>
                    <h4 class=\"description\">Look for further than these exquisite items, powered by STREES LUXURY's unrivaled designer network</h4>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-5 mr-auto ml-auto\">
                <div class=\"card-background\" style=\"min-height:500px;background-image: url(";
        // line 42
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner1.jpg");
        echo ")\">
                    <div class=\"card-body\">


                    </div>
                </div>
            </div>

                <div class=\"col-md-5 mr-auto ml-auto\">
                <div class=\"card-background\" style=\"min-height:500px;background-image: url(";
        // line 51
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner2.webp");
        echo ")\">
                    <div class=\"card-body\">


                    </div>
                </div>
                    
                

                </div>

            </div>
    </div>
    
    <!--     section 3     -->
    <div class=\"features-1\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 ml-auto mr-auto\">
                    <h2 class=\"title\">New in: hand-picked daily from the world’s best brands and boutiques</h2>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"";
        // line 79
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/shirt1.jpg");
        echo "\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>

                </div>

                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"";
        // line 107
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/shirt2.jpg");
        echo "\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>

                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"";
        // line 134
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/shirt3.jpg");
        echo "\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>
                
                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"";
        // line 161
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/shirt4.jpg");
        echo "\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>
                
            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet/themes/bouquet/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 161,  212 => 134,  182 => 107,  151 => 79,  120 => 51,  108 => 42,  65 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"page-header page-header-small\">
        <div class=\"page-header-image\" style=\"filter: brightness(80%);background-image: url({{ 'assets/img/banner.jpg'|theme }});\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-7 ml-auto text-right\">
                    <h1 class=\"title\">History of surfing</h1>
                    <h4 class=\"description\">The riding of waves has likely existed since humans began swimming in the ocean. In this sense, bodysurfing is the oldest type of wave-catching. Standing up on what is now called a surfboard is a relatively recent innovation developed by the Polynesians.</h4>
                    <br>

                    <div class=\"buttons\">
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-get-pocket\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-info btn-lg mr-3\">
                            Read More
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
      <!--     section 2     -->
    <div class=\"features-1\">

            <div class=\"row\">
                <div class=\"col-md-8 ml-auto mr-auto\">
                    <h2 class=\"title\">RARE GIFTS FOR YOUR FAVOURITE PEOPLE</h2>
                    <h4 class=\"description\">Look for further than these exquisite items, powered by STREES LUXURY's unrivaled designer network</h4>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-5 mr-auto ml-auto\">
                <div class=\"card-background\" style=\"min-height:500px;background-image: url({{ 'assets/img/banner1.jpg'|theme }})\">
                    <div class=\"card-body\">


                    </div>
                </div>
            </div>

                <div class=\"col-md-5 mr-auto ml-auto\">
                <div class=\"card-background\" style=\"min-height:500px;background-image: url({{ 'assets/img/banner2.webp'|theme }})\">
                    <div class=\"card-body\">


                    </div>
                </div>
                    
                

                </div>

            </div>
    </div>
    
    <!--     section 3     -->
    <div class=\"features-1\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-8 ml-auto mr-auto\">
                    <h2 class=\"title\">New in: hand-picked daily from the world’s best brands and boutiques</h2>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"{{ 'assets/img/shirt1.jpg'|theme }}\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>

                </div>

                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"{{ 'assets/img/shirt2.jpg'|theme }}\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>

                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"{{ 'assets/img/shirt3.jpg'|theme }}\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>
                
                <div class=\"col-md-3\">
                    <div class=\"card card-product card-plain\">
                                 <div class=\"card-image\" style=\"height:250px;\">
                                     <a href=\"#\">
                                         <img src=\"{{ 'assets/img/shirt4.jpg'|theme }}\" alt=\"...\">
                                     </a>
                                 </div>
                                 <div class=\"card-body\">
                                     <a href=\"#\">
                                         <h4 class=\"card-title\">Polo Ralph Lauren</h4>
                                     </a>
                                     <p class=\"card-description\">
                                        Impeccably tailored in Italy from lightweight navy wool.
                                     </p>
                                     <div class=\"card-footer\">
                                         <div class=\"price-container\">
                                            <span class=\"price\"> € 300</span>
                                         </div>

                                         <button class=\"btn btn-danger btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Remove from wishlist\">
                                             <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                         </button>
                                     </div>
                                 </div>
                             </div>
                </div>
                
            </div>
        </div>
    </div>", "C:\\xampp\\htdocs\\bouquet/themes/bouquet/pages/home.htm", "");
    }
}
