<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet/themes/bouquet/pages/detail.htm */
class __TwigTemplate_8000874f3e7bc6d1cec0730a430e469fbb6232823f557bd275c6d1a8cccabe4c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 3);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"wrapper\">
    <div class=\"page-header page-header-mini\">
    <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image: url(";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/bg7.jpg");
        echo "); transform: translate3d(0px, 0px, 0px);\">
    </div>
</div>



<div class=\"section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-5\">

                <div id=\"productCarousel\" class=\"carousel slide pointer-event\" data-ride=\"carousel\" data-interval=\"8000\">
                    <ol class=\"carousel-indicators\">
                        <li data-target=\"#productCarousel\" data-slide-to=\"0\" class=\"\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"1\" class=\"\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"2\" class=\"active\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"3\" class=\"\"></li>
                    </ol>
                    <div class=\"carousel-inner\" role=\"listbox\">
                        <div class=\"carousel-item\">
                            <img class=\"d-block img-raised\" src=\"";
        // line 23
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/bg3.jpg");
        echo "\" alt=\"First slide\">
                        </div>
                        <div class=\"carousel-item active carousel-item-left\">
                            <img class=\"d-block img-raised\" src=\"";
        // line 26
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/bg4.jpg");
        echo "\" alt=\"Second slide\">
                        </div>
                        <div class=\"carousel-item carousel-item-next carousel-item-left\">
                            <img class=\"d-block img-raised\" src=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/bg5.jpg");
        echo "\" alt=\"Third slide\">
                        </div>
                        <div class=\"carousel-item\">
                            <img class=\"d-block img-raised\" src=\"";
        // line 32
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/bg6.jpg");
        echo "\" alt=\"Third slide\">
                        </div>
                    </div>
                    <a class=\"carousel-control-prev\" href=\"#productCarousel\" role=\"button\" data-slide=\"prev\">
                        <button type=\"button\" class=\"btn btn-primary btn-icon btn-round btn-sm\" name=\"button\">
                            <i class=\"now-ui-icons arrows-1_minimal-left\"></i>
                        </button>
                    </a>
                    <a class=\"carousel-control-next\" href=\"#productCarousel\" role=\"button\" data-slide=\"next\">
                        <button type=\"button\" class=\"btn btn-primary btn-icon btn-round btn-sm\" name=\"button\">
                            <i class=\"now-ui-icons arrows-1_minimal-right\"></i>
                        </button>
                    </a>
                </div>

                <p class=\"blockquote blockquote-primary\">
                    \"And thank you for turning my personal jean jacket into a couture piece. Wear yours with mirrored sunglasses on vacation.\"<br><br>
                    <small>Kanye West</small>
                </p>

            </div>
            <div class=\"col-md-6 ml-auto mr-auto\">
                <h2 class=\"title\"> Saint Laurent </h2>
                <h5 class=\"category\">Slim-Fit Leather Biker Jacket</h5>
                <h2 class=\"main-price\">\$3,390</h2>

                <div id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\" class=\"card-collapse\">
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingOne\">
                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                            Description
                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>

                    <div id=\"collapseOne\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                      <div class=\"card-body\">
                        <p>Eres' daring 'Grigri Fortune' swimsuit has the fit and coverage of a bikini in a one-piece silhouette. This fuchsia style is crafted from the label's sculpting peau douce fabric and has flattering cutouts through the torso and back. Wear yours with mirrored sunglasses on vacation.</p>
                      </div>
                    </div>
                  </div>
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingTwo\">
                        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                            Designer Information

                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>
                    <div id=\"collapseTwo\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\" style=\"\">
                      <div class=\"card-body\">
                        <p>An infusion of West Coast cool and New York attitude, Rebecca Minkoff is synonymous with It girl style. Minkoff burst on the fashion scene with her best-selling 'Morning After Bag' and later expanded her offering with the Rebecca Minkoff Collection - a range of luxe city staples with a \"downtown romantic\" theme.</p>
                      </div>
                    </div>
                  </div>
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingThree\">
                        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                            Details and Care

                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>
                    <div id=\"collapseThree\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\" style=\"\">
                      <div class=\"card-body\">
                          <ul>
                               <li>Storm and midnight-blue stretch cotton-blend</li>
                               <li>Notch lapels, functioning buttoned cuffs, two front flap pockets, single vent, internal pocket</li>
                               <li>Two button fastening</li>
                               <li>84% cotton, 14% nylon, 2% elastane</li>
                               <li>Dry clean</li>
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div class=\"row pick-size\">
                    <div class=\"col-lg-6 col-md-8 col-sm-6\">
                        <label>Select color</label>
                        <div class=\"dropdown bootstrap-select\"><select class=\"selectpicker\" data-style=\"select-with-transition btn btn-block\" data-size=\"7\" tabindex=\"-98\">
                            <option value=\"1\">Black</option>
                            <option value=\"2\">Gray</option>
                            <option value=\"3\">White</option>
                        </select><button type=\"button\" class=\"dropdown-toggle select-with-transition btn btn-block\" data-toggle=\"dropdown\" role=\"button\" title=\"Black\"><div class=\"filter-option\"><div class=\"filter-option-inner\"><div class=\"filter-option-inner-inner\">Black</div></div> </div></button><div class=\"dropdown-menu \" role=\"combobox\"><div class=\"inner show\" role=\"listbox\" aria-expanded=\"false\" tabindex=\"-1\"><ul class=\"dropdown-menu inner show\"></ul></div></div></div>
                    </div>
                    <div class=\"col-lg-6 col-md-8 col-sm-6\">
                        <label>Select size</label>
                        <div class=\"dropdown bootstrap-select\"><select class=\"selectpicker\" data-style=\"select-with-transition btn btn-block\" data-size=\"7\" tabindex=\"-98\">
                            <option value=\"1\">Small </option>
                            <option value=\"2\">Medium</option>
                            <option value=\"3\">Large</option>
                        </select><button type=\"button\" class=\"dropdown-toggle select-with-transition btn btn-block\" data-toggle=\"dropdown\" role=\"button\" title=\"Small\"><div class=\"filter-option\"><div class=\"filter-option-inner\"><div class=\"filter-option-inner-inner\">Small</div></div> </div></button><div class=\"dropdown-menu \" role=\"combobox\"><div class=\"inner show\" role=\"listbox\" aria-expanded=\"false\" tabindex=\"-1\"><ul class=\"dropdown-menu inner show\"></ul></div></div></div>
                    </div>
                </div>
                <div class=\"row justify-content-end\">
                    <button class=\"btn btn-primary mr-3\">Add to Cart &nbsp;<i class=\"now-ui-icons shopping_cart-simple\"></i></button>
                </div>
            </div>
        </div>

        

        <div class=\"features-4\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8 ml-auto mr-auto text-center\">
                        <h2 class=\"title\">Not convinced yet!</h2>
                        <h4 class=\"description\">Havenly is a convenient, personal and affordable way to redecorate your home room by room. Collaborate with our professional interior designers on our online platform. </h4>
                    </div>
                </div>

                <div class=\"row\">
                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg24.jpg')\">
                            <div class=\"info\">
                                <div class=\"icon icon-white\">
                                    <i class=\"now-ui-icons shopping_delivery-fast\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">1 Day Delivery </h4>
                                    <p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
                                    <a href=\"#pablo\" class=\"ml-3\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg28.jpg')\">
                            <div class=\"info\">
                                <div class=\"icon icon-white\">
                                    <i class=\"now-ui-icons business_badge\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">Refund Policy</h4>
                                    <p>Divide details about your product or agency work into parts. Write a few lines about each one. Very good refund policy just for you.</p>
                                    <a href=\"#pablo\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg25.jpg')\">
                            <div class=\"info\">

                                <div class=\"icon\">
                                    <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">Popular Item</h4>
                                    <p>Share a floor plan, and we'll create a visualization of your room. A paragraph describing a feature will be enough. This is a popular item for you.</p>
                                    <a href=\"#pablo\" class=\"ml-3\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class=\"section related-products\" data-background-color=\"black\">
    <div class=\"container\">
        <h3 class=\"title text-center\">You may also be interested in:</h3>

        <div class=\"row\">
            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/saint-laurent.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-danger\">Trending</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Dolce &amp; Gabbana</a>
                        </h4>
                        <div class=\"card-description\">
                            Dolce &amp; Gabbana's 'Greta' tote has been crafted in Italy from hard-wearing red textured-leather.
                        </div>


                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€1,459</span>
                             </div>
                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>

                    </div>

                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/gucci.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-muted\">Popular</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Balmain</a>
                        </h4>
                        <div class=\"card-description\">
                            Balmain's mid-rise skinny jeans are cut with stretch to ensure they retain their second-skin fit but move comfortably.
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€459</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>


                    </div>

                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/wooyoungmi.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-muted\">Popular</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Balenciaga</a>
                        </h4>
                        <div class=\"card-description\">
                            Balenciaga's black textured-leather wallet is finished with the label's iconic 'Giant' studs. This is where you can...
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€559</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/saint-laurent1.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-rose\">Trending</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Dolce &amp; Gabbana</a>
                        </h4>
                        <div class=\"card-description\">
                            Dolce &amp; Gabbana's 'Greta' tote has been crafted in Italy from hard-wearing red textured-leather.
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\"> € 1,359</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


                <footer class=\"footer\">
    
    <div class=\"container\">

      <div class=\"content\">
        <div class=\"row\">

          <div class=\"col-md-3\">
            <a href=\"#pablo\"><h5>Now Ui Kit PRO</h5></a>
            <p>Probably the best UI Kit in the world! We know you've been waiting for it, so don't be shy!</p>
          </div>
          <div class=\"col-md-2\">
            <h5>About</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Blog
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   About Us
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Presentation
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Contact Us
                </a>
              </li>
            </ul>
          </div>
          <div class=\"col-md-2\">
            <h5>Market</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Sales FAQ
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  How to Register
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   Sell Goods
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Receive Payment
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Transactions Issues
                </a>
              </li>
            </ul>
          </div>

          <div class=\"col-md-2\">
            <h5>Legal</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Transactions FAQ
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Terms &amp; Conditions
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   Licenses
                </a>
              </li>
            </ul>
          </div>
          <div class=\"col-md-3\">
            <h5>Subscribe to Newsletter</h5>
            <p>
              Join our newsletter and get news in your inbox every week! We hate spam too, so no worries about this.
            </p>
            <form class=\"form form-newsletter\" method=\"\" action=\"\">
        <div class=\"form-group\">
          <input type=\"email\" class=\"form-control\" placeholder=\"Your Email...\">
        </div>

              <button type=\"button\" class=\"btn btn-primary btn-icon btn-round\" name=\"button\">
                <i class=\"now-ui-icons ui-1_email-85\"></i>
              </button>
            </form>
          </div>

        </div>
      </div>

      <hr>

      <ul class=\"social-buttons\">
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-twitter btn-lg\">
            <i class=\"fab fa-twitter\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-facebook btn-lg\">
            <i class=\"fab fa-facebook-square\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-dribbble btn-lg\">
            <i class=\"fab fa-dribbble\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-google btn-lg\">
            <i class=\"fab fa-google-plus\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-youtube btn-lg\">
            <i class=\"fab fa-youtube\"></i>
          </a>
        </li>
      </ul>

      <div class=\"copyright pull-center\" id=\"copyright\">
        Copyright © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2020 Creative Tim All Rights Reserved.
      </div>
    </div>
    


</footer>

            </div>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet/themes/bouquet/pages/detail.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 32,  101 => 29,  95 => 26,  89 => 23,  66 => 3,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"wrapper\">
    <div class=\"page-header page-header-mini\">
    <div class=\"page-header-image\" data-parallax=\"true\" style=\"background-image: url({{ 'assets/img/bg7.jpg'|theme }}); transform: translate3d(0px, 0px, 0px);\">
    </div>
</div>



<div class=\"section\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-5\">

                <div id=\"productCarousel\" class=\"carousel slide pointer-event\" data-ride=\"carousel\" data-interval=\"8000\">
                    <ol class=\"carousel-indicators\">
                        <li data-target=\"#productCarousel\" data-slide-to=\"0\" class=\"\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"1\" class=\"\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"2\" class=\"active\"></li>
                        <li data-target=\"#productCarousel\" data-slide-to=\"3\" class=\"\"></li>
                    </ol>
                    <div class=\"carousel-inner\" role=\"listbox\">
                        <div class=\"carousel-item\">
                            <img class=\"d-block img-raised\" src=\"{{ 'assets/img/bg3.jpg'|theme }}\" alt=\"First slide\">
                        </div>
                        <div class=\"carousel-item active carousel-item-left\">
                            <img class=\"d-block img-raised\" src=\"{{ 'assets/img/bg4.jpg'|theme }}\" alt=\"Second slide\">
                        </div>
                        <div class=\"carousel-item carousel-item-next carousel-item-left\">
                            <img class=\"d-block img-raised\" src=\"{{ 'assets/img/bg5.jpg'|theme }}\" alt=\"Third slide\">
                        </div>
                        <div class=\"carousel-item\">
                            <img class=\"d-block img-raised\" src=\"{{ 'assets/img/bg6.jpg'|theme }}\" alt=\"Third slide\">
                        </div>
                    </div>
                    <a class=\"carousel-control-prev\" href=\"#productCarousel\" role=\"button\" data-slide=\"prev\">
                        <button type=\"button\" class=\"btn btn-primary btn-icon btn-round btn-sm\" name=\"button\">
                            <i class=\"now-ui-icons arrows-1_minimal-left\"></i>
                        </button>
                    </a>
                    <a class=\"carousel-control-next\" href=\"#productCarousel\" role=\"button\" data-slide=\"next\">
                        <button type=\"button\" class=\"btn btn-primary btn-icon btn-round btn-sm\" name=\"button\">
                            <i class=\"now-ui-icons arrows-1_minimal-right\"></i>
                        </button>
                    </a>
                </div>

                <p class=\"blockquote blockquote-primary\">
                    \"And thank you for turning my personal jean jacket into a couture piece. Wear yours with mirrored sunglasses on vacation.\"<br><br>
                    <small>Kanye West</small>
                </p>

            </div>
            <div class=\"col-md-6 ml-auto mr-auto\">
                <h2 class=\"title\"> Saint Laurent </h2>
                <h5 class=\"category\">Slim-Fit Leather Biker Jacket</h5>
                <h2 class=\"main-price\">\$3,390</h2>

                <div id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\" class=\"card-collapse\">
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingOne\">
                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\" aria-expanded=\"true\" aria-controls=\"collapseOne\">
                            Description
                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>

                    <div id=\"collapseOne\" class=\"collapse show\" role=\"tabpanel\" aria-labelledby=\"headingOne\">
                      <div class=\"card-body\">
                        <p>Eres' daring 'Grigri Fortune' swimsuit has the fit and coverage of a bikini in a one-piece silhouette. This fuchsia style is crafted from the label's sculpting peau douce fabric and has flattering cutouts through the torso and back. Wear yours with mirrored sunglasses on vacation.</p>
                      </div>
                    </div>
                  </div>
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingTwo\">
                        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\" aria-expanded=\"false\" aria-controls=\"collapseTwo\">
                            Designer Information

                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>
                    <div id=\"collapseTwo\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\" style=\"\">
                      <div class=\"card-body\">
                        <p>An infusion of West Coast cool and New York attitude, Rebecca Minkoff is synonymous with It girl style. Minkoff burst on the fashion scene with her best-selling 'Morning After Bag' and later expanded her offering with the Rebecca Minkoff Collection - a range of luxe city staples with a \"downtown romantic\" theme.</p>
                      </div>
                    </div>
                  </div>
                  <div class=\"card card-plain\">
                    <div class=\"card-header\" role=\"tab\" id=\"headingThree\">
                        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseThree\" aria-expanded=\"false\" aria-controls=\"collapseThree\">
                            Details and Care

                            <i class=\"now-ui-icons arrows-1_minimal-down\"></i>
                        </a>
                    </div>
                    <div id=\"collapseThree\" class=\"collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\" style=\"\">
                      <div class=\"card-body\">
                          <ul>
                               <li>Storm and midnight-blue stretch cotton-blend</li>
                               <li>Notch lapels, functioning buttoned cuffs, two front flap pockets, single vent, internal pocket</li>
                               <li>Two button fastening</li>
                               <li>84% cotton, 14% nylon, 2% elastane</li>
                               <li>Dry clean</li>
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>

                <div class=\"row pick-size\">
                    <div class=\"col-lg-6 col-md-8 col-sm-6\">
                        <label>Select color</label>
                        <div class=\"dropdown bootstrap-select\"><select class=\"selectpicker\" data-style=\"select-with-transition btn btn-block\" data-size=\"7\" tabindex=\"-98\">
                            <option value=\"1\">Black</option>
                            <option value=\"2\">Gray</option>
                            <option value=\"3\">White</option>
                        </select><button type=\"button\" class=\"dropdown-toggle select-with-transition btn btn-block\" data-toggle=\"dropdown\" role=\"button\" title=\"Black\"><div class=\"filter-option\"><div class=\"filter-option-inner\"><div class=\"filter-option-inner-inner\">Black</div></div> </div></button><div class=\"dropdown-menu \" role=\"combobox\"><div class=\"inner show\" role=\"listbox\" aria-expanded=\"false\" tabindex=\"-1\"><ul class=\"dropdown-menu inner show\"></ul></div></div></div>
                    </div>
                    <div class=\"col-lg-6 col-md-8 col-sm-6\">
                        <label>Select size</label>
                        <div class=\"dropdown bootstrap-select\"><select class=\"selectpicker\" data-style=\"select-with-transition btn btn-block\" data-size=\"7\" tabindex=\"-98\">
                            <option value=\"1\">Small </option>
                            <option value=\"2\">Medium</option>
                            <option value=\"3\">Large</option>
                        </select><button type=\"button\" class=\"dropdown-toggle select-with-transition btn btn-block\" data-toggle=\"dropdown\" role=\"button\" title=\"Small\"><div class=\"filter-option\"><div class=\"filter-option-inner\"><div class=\"filter-option-inner-inner\">Small</div></div> </div></button><div class=\"dropdown-menu \" role=\"combobox\"><div class=\"inner show\" role=\"listbox\" aria-expanded=\"false\" tabindex=\"-1\"><ul class=\"dropdown-menu inner show\"></ul></div></div></div>
                    </div>
                </div>
                <div class=\"row justify-content-end\">
                    <button class=\"btn btn-primary mr-3\">Add to Cart &nbsp;<i class=\"now-ui-icons shopping_cart-simple\"></i></button>
                </div>
            </div>
        </div>

        

        <div class=\"features-4\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-8 ml-auto mr-auto text-center\">
                        <h2 class=\"title\">Not convinced yet!</h2>
                        <h4 class=\"description\">Havenly is a convenient, personal and affordable way to redecorate your home room by room. Collaborate with our professional interior designers on our online platform. </h4>
                    </div>
                </div>

                <div class=\"row\">
                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg24.jpg')\">
                            <div class=\"info\">
                                <div class=\"icon icon-white\">
                                    <i class=\"now-ui-icons shopping_delivery-fast\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">1 Day Delivery </h4>
                                    <p>Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough.</p>
                                    <a href=\"#pablo\" class=\"ml-3\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg28.jpg')\">
                            <div class=\"info\">
                                <div class=\"icon icon-white\">
                                    <i class=\"now-ui-icons business_badge\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">Refund Policy</h4>
                                    <p>Divide details about your product or agency work into parts. Write a few lines about each one. Very good refund policy just for you.</p>
                                    <a href=\"#pablo\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=\"col-md-4\">
                        <div class=\"card card-background card-raised\" data-background-color=\"\" style=\"background-image: url('../assets/img/bg25.jpg')\">
                            <div class=\"info\">

                                <div class=\"icon\">
                                    <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                                </div>
                                <div class=\"description\">
                                    <h4 class=\"info-title\">Popular Item</h4>
                                    <p>Share a floor plan, and we'll create a visualization of your room. A paragraph describing a feature will be enough. This is a popular item for you.</p>
                                    <a href=\"#pablo\" class=\"ml-3\">Find more...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class=\"section related-products\" data-background-color=\"black\">
    <div class=\"container\">
        <h3 class=\"title text-center\">You may also be interested in:</h3>

        <div class=\"row\">
            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/saint-laurent.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-danger\">Trending</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Dolce &amp; Gabbana</a>
                        </h4>
                        <div class=\"card-description\">
                            Dolce &amp; Gabbana's 'Greta' tote has been crafted in Italy from hard-wearing red textured-leather.
                        </div>


                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€1,459</span>
                             </div>
                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>

                    </div>

                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/gucci.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-muted\">Popular</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Balmain</a>
                        </h4>
                        <div class=\"card-description\">
                            Balmain's mid-rise skinny jeans are cut with stretch to ensure they retain their second-skin fit but move comfortably.
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€459</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>


                    </div>

                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/wooyoungmi.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-muted\">Popular</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Balenciaga</a>
                        </h4>
                        <div class=\"card-description\">
                            Balenciaga's black textured-leather wallet is finished with the label's iconic 'Giant' studs. This is where you can...
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\">€559</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>
                    </div>
                </div>
            </div>

            <div class=\"col-sm-6 col-md-3\">
                <div class=\"card card-product\">
                    <div class=\"card-image\">
                        <a href=\"#pablo\">
                            <img class=\"img rounded\" src=\"../assets/img/saint-laurent1.jpg\">
                        </a>
                    </div>

                    <div class=\"card-body\">
                        <h6 class=\"category text-rose\">Trending</h6>
                        <h4 class=\"card-title\">
                            <a href=\"#pablo\" class=\"card-link\">Dolce &amp; Gabbana</a>
                        </h4>
                        <div class=\"card-description\">
                            Dolce &amp; Gabbana's 'Greta' tote has been crafted in Italy from hard-wearing red textured-leather.
                        </div>
                        <div class=\"card-footer\">
                             <div class=\"price-container\">
                                <span class=\"price\"> € 1,359</span>
                             </div>

                             <button class=\"btn btn-neutral btn-icon btn-round pull-right\" rel=\"tooltip\" title=\"\" data-placement=\"left\" data-original-title=\"Add to wishlist\">
                                 <i class=\"now-ui-icons ui-2_favourite-28\"></i>
                             </button>
                         </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>


                <footer class=\"footer\">
    
    <div class=\"container\">

      <div class=\"content\">
        <div class=\"row\">

          <div class=\"col-md-3\">
            <a href=\"#pablo\"><h5>Now Ui Kit PRO</h5></a>
            <p>Probably the best UI Kit in the world! We know you've been waiting for it, so don't be shy!</p>
          </div>
          <div class=\"col-md-2\">
            <h5>About</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Blog
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   About Us
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Presentation
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Contact Us
                </a>
              </li>
            </ul>
          </div>
          <div class=\"col-md-2\">
            <h5>Market</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Sales FAQ
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  How to Register
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   Sell Goods
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Receive Payment
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Transactions Issues
                </a>
              </li>
            </ul>
          </div>

          <div class=\"col-md-2\">
            <h5>Legal</h5>
            <ul class=\"links-vertical\">
              <li>
                <a href=\"#pablo\">
                   Transactions FAQ
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                  Terms &amp; Conditions
                </a>
              </li>
              <li>
                <a href=\"#pablo\">
                   Licenses
                </a>
              </li>
            </ul>
          </div>
          <div class=\"col-md-3\">
            <h5>Subscribe to Newsletter</h5>
            <p>
              Join our newsletter and get news in your inbox every week! We hate spam too, so no worries about this.
            </p>
            <form class=\"form form-newsletter\" method=\"\" action=\"\">
        <div class=\"form-group\">
          <input type=\"email\" class=\"form-control\" placeholder=\"Your Email...\">
        </div>

              <button type=\"button\" class=\"btn btn-primary btn-icon btn-round\" name=\"button\">
                <i class=\"now-ui-icons ui-1_email-85\"></i>
              </button>
            </form>
          </div>

        </div>
      </div>

      <hr>

      <ul class=\"social-buttons\">
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-twitter btn-lg\">
            <i class=\"fab fa-twitter\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-facebook btn-lg\">
            <i class=\"fab fa-facebook-square\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-dribbble btn-lg\">
            <i class=\"fab fa-dribbble\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-google btn-lg\">
            <i class=\"fab fa-google-plus\"></i>
          </a>
        </li>
        <li>
          <a href=\"#pablo\" class=\"btn btn-icon btn-neutral btn-youtube btn-lg\">
            <i class=\"fab fa-youtube\"></i>
          </a>
        </li>
      </ul>

      <div class=\"copyright pull-center\" id=\"copyright\">
        Copyright © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2020 Creative Tim All Rights Reserved.
      </div>
    </div>
    


</footer>

            </div>", "C:\\xampp\\htdocs\\bouquet/themes/bouquet/pages/detail.htm", "");
    }
}
