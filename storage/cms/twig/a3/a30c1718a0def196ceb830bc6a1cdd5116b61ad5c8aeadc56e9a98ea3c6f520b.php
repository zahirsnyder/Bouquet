<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet/themes/bouquet/partials/nav.htm */
class __TwigTemplate_4ff5ecfefa01b8339771c27d51c3cd3c7ed0e920e6696640ed67520ba63a15bc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<nav class=\"navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute\">
        <div class=\"container\">
            <div class=\"navbar-translate\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-bar bar1\"></span>
                    <span class=\"navbar-toggler-bar bar2\"></span>
                    <span class=\"navbar-toggler-bar bar3\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"#pablo\">Street Luxury</a>
            </div>

            <div class=\"collapse navbar-collapse\">
                <ul class=\"navbar-nav mx-auto\">
                    <li class=\"nav-item active\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            Home
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            About Us
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            Contact Us
                        </a>
                    </li>
                </ul>

                <ul class=\"nav navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://twitter.com/CreativeTim\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://www.facebook.com/CreativeTim\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://www.instagram.com/CreativeTimOfficial\">
                            <i class=\"fab fa-instagram\"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet/themes/bouquet/partials/nav.htm";
    }

    public function getDebugInfo()
    {
        return array (  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<nav class=\"navbar navbar-expand-lg navbar-transparent bg-primary navbar-absolute\">
        <div class=\"container\">
            <div class=\"navbar-translate\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-bar bar1\"></span>
                    <span class=\"navbar-toggler-bar bar2\"></span>
                    <span class=\"navbar-toggler-bar bar3\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"#pablo\">Street Luxury</a>
            </div>

            <div class=\"collapse navbar-collapse\">
                <ul class=\"navbar-nav mx-auto\">
                    <li class=\"nav-item active\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            Home
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            About Us
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"#pablo\">
                            Contact Us
                        </a>
                    </li>
                </ul>

                <ul class=\"nav navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://twitter.com/CreativeTim\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://www.facebook.com/CreativeTim\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"https://www.instagram.com/CreativeTimOfficial\">
                            <i class=\"fab fa-instagram\"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>", "C:\\xampp\\htdocs\\bouquet/themes/bouquet/partials/nav.htm", "");
    }
}
