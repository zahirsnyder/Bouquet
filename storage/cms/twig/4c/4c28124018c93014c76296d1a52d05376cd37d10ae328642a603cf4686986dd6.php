<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet/themes/bouquet/partials/site/script.htm */
class __TwigTemplate_bf2a96d6a152ddf7f72ab0e2e073d520b1b30bf67f98769abd9ea7ec90f5232d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 1);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/core/jquery.min.js");
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/core/popper.min.js");
        echo "\" type=\"text/javascript\"></script>
  <script src=\"";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/core/bootstrap.min.js");
        echo "\" type=\"text/javascript\"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap-switch.js");
        echo "\"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/nouislider.min.js");
        echo "\" type=\"text/javascript\"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/plugins/bootstrap-datepicker.js");
        echo "\" type=\"text/javascript\"></script>
  <!--  Google Maps Plugin    -->
  <script src=\"https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE\"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/now-ui-kit.js?v=1.3.0");
        echo "\" type=\"text/javascript\"></script>



<script>
  \$(document).ready(function () {
    // the body of this function is in assets/js/now-ui-kit.js
    nowuiKit.initSliders();
  });

  function scrollToDownload() {

    if (\$('.section-download').length != 0) {
      \$(\"html, body\").animate({
        scrollTop: \$('.section-download').offset().top
      }, 1000);
    }
  }
</script>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet/themes/bouquet/partials/site/script.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 13,  86 => 9,  81 => 7,  76 => 5,  71 => 3,  67 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script src=\"{{ 'assets/js/core/jquery.min.js'|theme }}\" type=\"text/javascript\"></script>
  <script src=\"{{ 'assets/js/core/popper.min.js'|theme }}\" type=\"text/javascript\"></script>
  <script src=\"{{ 'assets/js/core/bootstrap.min.js'|theme }}\" type=\"text/javascript\"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src=\"{{ 'assets/js/plugins/bootstrap-switch.js'|theme }}\"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src=\"{{ 'assets/js/plugins/nouislider.min.js'|theme }}\" type=\"text/javascript\"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src=\"{{ 'assets/js/plugins/bootstrap-datepicker.js'|theme }}\" type=\"text/javascript\"></script>
  <!--  Google Maps Plugin    -->
  <script src=\"https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE\"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src=\"{{ 'assets/js/now-ui-kit.js?v=1.3.0'|theme }}\" type=\"text/javascript\"></script>



<script>
  \$(document).ready(function () {
    // the body of this function is in assets/js/now-ui-kit.js
    nowuiKit.initSliders();
  });

  function scrollToDownload() {

    if (\$('.section-download').length != 0) {
      \$(\"html, body\").animate({
        scrollTop: \$('.section-download').offset().top
      }, 1000);
    }
  }
</script>", "C:\\xampp\\htdocs\\bouquet/themes/bouquet/partials/site/script.htm", "");
    }
}
