<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\bouquet-takjadi/themes/bouquet/pages/home.htm */
class __TwigTemplate_57c95c6d059e30743e2efe372482f8aca84e2aeedb19ad46c8029fba9735b156 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 2);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"page-header header-filter\">
        <div class=\"page-header-image\" style=\"background-image: url(";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/header.jpg");
        echo ");\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-7 ml-auto text-right\">
                    <h1 class=\"title\">History of surfing</h1>
                    <h4 class=\"description\">The riding of waves has likely existed since humans began swimming in the ocean. In this sense, bodysurfing is the oldest type of wave-catching. Standing up on what is now called a surfboard is a relatively recent innovation developed by the Polynesians.</h4>
                    <br>

                    <div class=\"buttons\">
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-get-pocket\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-info btn-lg mr-3\">
                            Read More
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\bouquet-takjadi/themes/bouquet/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"page-header header-filter\">
        <div class=\"page-header-image\" style=\"background-image: url({{ 'assets/img/header.jpg'|theme }});\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-md-7 ml-auto text-right\">
                    <h1 class=\"title\">History of surfing</h1>
                    <h4 class=\"description\">The riding of waves has likely existed since humans began swimming in the ocean. In this sense, bodysurfing is the oldest type of wave-catching. Standing up on what is now called a surfboard is a relatively recent innovation developed by the Polynesians.</h4>
                    <br>

                    <div class=\"buttons\">
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-twitter\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-facebook-square\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-icon btn-link btn-neutral btn-lg\">
                            <i class=\"fab fa-get-pocket\"></i>
                        </a>
                        <a href=\"#pablo\" class=\"btn btn-info btn-lg mr-3\">
                            Read More
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>", "C:\\xampp\\htdocs\\bouquet-takjadi/themes/bouquet/pages/home.htm", "");
    }
}
